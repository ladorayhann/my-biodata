from django.test import SimpleTestCase
from django.urls import reverse, resolve
from hometeam.views import homepage, team

class TestUrls(SimpleTestCase):

    def test_homepage_url_is_resolved(self):
        url = reverse('hometeam:homepage')
        self.assertEquals(resolve(url).func , homepage)

    def test_team_url_is_resolved(self):
        url = reverse('hometeam:team')
        self.assertEquals(resolve(url).func , team)
